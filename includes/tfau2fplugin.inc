<?php
use u2flib_server\Error;

class TfaU2fPlugin extends TfaBasePlugin implements TfaValidationPluginInterface {

  /**
   * Get TFA process form from plugin.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array Form API array.
   */
  public function getForm(array $form, array &$form_state) {

    // Output the information we need as javascript.
    drupal_add_js(drupal_get_path('module', 'tfa_u2f') .'/u2f-api.js', 'file');

    // Instansiate the U2F utility class.
    // TODO: The app-id should be unique, also across www/non-www, eg a setting.
    $u2f = tfa_u2f_get_u2f();

    $data = tfa_u2f_get_registrations_for_user($this->context['uid']);
    $reqs = $u2f->getAuthenticateData($data);

    if (empty($_SESSION['authReq'])) {
      $_SESSION['authReq'] = json_encode($reqs);
    }

    $js_data['u2f_sign'] = array(
      'req' => $reqs,
      'uid' => $this->context['uid'],
    );

    $form['info'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t('Sign with your U2F device.') . '</p>',
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'tfa_u2f') . '/tfa_u2f.js',
          array('data' => $js_data, 'type' => 'setting'),
        ),
        'library' => array(array('system', 'ui.sortable')),
      ),
    );
    $form['sign'] = array(
      '#attributes' => array('class' => array('u2f-sign')),
      '#type' => 'submit',
      '#value' => t('Sign'),
      '#name' => 'u2f-sign',
    );
    $form['signdata'] = array(
      '#type' => 'hidden',
      '#name' => 'signdata',
    );

    return $form;
  }

  /**
   * Validate form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return bool Whether form passes validation or not
   */
  public function validateForm(array $form, array &$form_state) {
    // Do the verification stuff here
    $bla = 123;
    $u2f = tfa_u2f_get_u2f();
    $original_reqs = json_decode($_SESSION['authReq']);
    $_SESSION['authReq'] = NULL;
    $signed_req = json_decode($form_state['values']['signdata']);
    $current_reqs = tfa_u2f_get_registrations_for_user($this->context['uid']);
    try{
      $reg = $u2f->doAuthenticate($original_reqs, $current_reqs, $signed_req);
      // $reg = $u2f->doAuthenticate(json_decode($_SESSION['authReq']), getRegs($user->id), json_decode($_POST['authenticate2']));
      variable_set('tfa_u2f_' . $this->context['uid'], serialize($reg));
      $this->isValid = TRUE;
    } catch (Error $e) {
      throw $e;
//      $this->isValid = FALSE;
    }

    return $this->isValid;
    // TODO: Implement validateForm() method.
  }

}


/**
 * Class TfaBasicSmsSetup
 */
class TfaU2fSetup extends TfaU2fPlugin implements TfaSetupPluginInterface {

  public function __construct(array $context, $mobile_number) {
    parent::__construct($context, $mobile_number);
  }

  public function begin() {
    if (empty($this->code)) {
      $this->code = $this->generate();
      if (!$this->sendCode($this->code)) {
        // @todo decide on error text
        $this->errorMessages[''] = t('Unable to deliver code to that number.');
      }
    }
  }

  /**
   * @copydoc TfaSetupPluginInterface::getSetupForm()
   */
  public function getSetupForm(array $form, array &$form_state) {
    $form['code'] = array(
      '#type' => 'textfield',
      '#title' => t('Verification Code'),
      '#required' => TRUE,
      '#description' => t('Enter @length-character code sent to your device.', array('@length' => $this->codeLength)),
    );
    $form['actions']['verify'] = array(
      '#type' => 'submit',
      '#value' => t('Verify and save'),
    );

    return $form;
  }

  /**
   * @copydoc TfaSetupPluginInterface::validateSetupForm()
   */
  public function validateSetupForm(array $form, array &$form_state) {
    if (!$this->validate($form_state['values']['code'])) {
      $this->errorMessages['code'] = t('Invalid code. Please try again.');
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * @copydoc TfaSetupPluginInterface::submitSetupForm()
   */
  public function submitSetupForm(array $form, array &$form_state) {
    // No submission handling required.
    return TRUE;
  }

}

