(function ($) {
  Drupal.behaviors.tfa_u2f = {
    attach: function (context, settings) {
      console.log("attached")
      $('.u2f-register:not(.button-processed)', context)
        .addClass('button-processed').each(function () {
          $(this).click(function () {

            if (typeof Drupal.settings.u2f_reg === 'undefined'
             || typeof Drupal.settings.u2f_reg.sigs === 'undefined'
             || typeof Drupal.settings.u2f_reg.req === 'undefined'
             || typeof Drupal.settings.u2f_reg.uid === 'undefined'
            ) {
              // TODO: error-message to user.
              return false;
            }

            var reg_button = this;
            var callback = function(data) {
              console.log(reg_button);
              console.log("Register callback", data);
              if(data.errorCode) {
                alert("registration failed with errror: " + data.errorCode);
                return;
              }
              $('input[name="regdata"]', reg_button.form).val(JSON.stringify(data));

              reg_button.form.submit();
            };

            console.log("Register: ", Drupal.settings.u2f_reg.req);
            u2f.register([Drupal.settings.u2f_reg.req], Drupal.settings.u2f_reg.sigs, callback);

            return false;

          });

          return false;
        });
    }
  };

  Drupal.behaviors.tfa_u2f_sign = {
    attach: function (context, settings) {
      console.log("attached_sign")
      $('.u2f-sign:not(.button-processed)', context)
        .addClass('button-processed').each(function () {
          $(this).click(function () {
            debugger;

            if (typeof Drupal.settings.u2f_sign === 'undefined'
              || typeof Drupal.settings.u2f_sign.req === 'undefined'
              || typeof Drupal.settings.u2f_sign.uid === 'undefined'
            ) {
              // TODO: error-message to user.
              return false;
            }

            var sign_button = this;

            console.log("sign: ", Drupal.settings.u2f_sign.req);

            var callback = function(data) {

              console.log(sign_button);
              console.log("Sign callback sign", data);
              $('input[name="signdata"]', sign_button.form).val(JSON.stringify(data));

              sign_button.form.submit();
            };
            u2f.sign(Drupal.settings.u2f_sign.req, callback);

            return false;

          });

          return false;
        });
    }
  };
})(jQuery);


